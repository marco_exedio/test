currentBuild.result = 'SUCCESS';

node('master') {
	
	def isMaster = false;
    def isRelease = false;
    def isFeature = false;
    
    try
    {
		def workspace = env.WORKSPACE;
		def jenkins_home = env.JENKINS_HOME;
    	def branchName = env.BRANCH_NAME;
	    isMaster = branchName.toString() == "master";
	    def isDevExedio = branchName.toString() == "dev-exedio";
	    isRelease = isReleaseBranch();
	    isFeature = !(isMaster || isRelease || isDevExedio);
	    def jobName = env.JOB_NAME;
        def buildNumber = env.BUILD_NUMBER;
        def mvnHome = tool name: 'mvn', type: 'maven';
        env.PATH = "${mvnHome}/bin:${env.PATH}";

	    properties([[$class: 'GogsProjectProperty', gogsSecret: 'top_s3cret!', gogsUsePayload: false], pipelineTriggers([[$class: 'BitBucketTrigger']],), buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '10', daysToKeepStr: '', numToKeepStr: '')) ])

		echo("Branch: $branchName; master: $isMaster release: $isRelease dev-exedio: $isDevExedio")
    	setBuildName()
        sendMessageViaSlack("Build Started - ${env.JOB_NAME}  (<${env.BUILD_URL}|Open>)")


		stage('Checkout & CleanUp'){
			deleteDir()
			checkoutFomGit()
        }

        stage('Descompact platform'){
			sh "mkdir $workspace/tmp"
			sh "unzip $jenkins_home/platforms/CXCOMM190500P_0-70004140.ZIP -d $workspace/tmp"
            sh("cp -r $workspace/tmp/hybris/bin/platform/resources/configtemplates/develop $workspace/tmp/hybris/config/") // TODO add check if we are deploying in production env, then we need change the config template
		}

		stage('Prepare enviroment'){
            sh "cp -r $workspace/config/local.properties $workspace/tmp/hybris/config/"
            sh "cp -r $workspace/config/localextensions.xml $workspace/tmp/hybris/config/"
            sh "cp -r $workspace/bin/custom $workspace/tmp/hybris/bin/"
        }

		stage('Build'){
            antStage("clean all")
            antStage("reinstall_addons -Dtarget.storefront=grupolevelstorefront")
		}

        stage('Archive Artifacts'){
            if(!isFeature){
                antStage("production -Dproduction.legacy.mode=false")
			    archiveArtifacts 'tmp/hybris/temp/hybris/hybrisServer/*.zip'
            }else{
                echo "skipping archive artifacts because branch is a feature"
            }
		}
		
    }catch(Exception e)
    {
        //todo handle script returned exit code 143
        currentBuild.result = 'FAILURE'
        throw e;
    }finally
    {
    	cleanWs()
        sendMessageViaSlack("Build Finish - ${env.JOB_NAME} with status: ${currentBuild.result} (<${env.BUILD_URL}|Open>)")
    }
}

def sendMessageViaSlack(message){

    def channel = "builds"
    if ('FAILURE'.equals(currentBuild.result)) {
        channel = "alerts"
    }

    slackSend baseUrl: 'https://exediobr.slack.com/services/hooks/jenkins-ci/', channel: "$channel", message: "$message", tokenCredentialId: 'jenkins-slack-integration'
}

def checkoutFomGit(){
		checkout scm
}

def getRepository(gitUrl, folderTarget){
	checkout([$class: 'GitSCM', branches: [[name: '*/master']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "$folderTarget"]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'b0864a31-0a3a-48fd-be36-8f15e5d0665a', url: "$gitUrl" ]]])
}

def getRepository(gitUrl, folderTarget, branch){
    checkout([$class: 'GitSCM', branches: [[name: "*/$branch"]], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "$folderTarget"]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'b0864a31-0a3a-48fd-be36-8f15e5d0665a', url: "$gitUrl" ]]])
}

def antStage(antTarget) 
{
	def workspace = env.WORKSPACE;
    
    withAnt(installation: 'ant10', jdk: 'JDK11', buildFile:'$workspace/tmp/hybris/bin/platform'){
	    sh "cd $workspace/tmp/hybris/bin/platform; ant ${antTarget}"
	}
}

def mvnStage(mvnTarget) 
{
    def workspace = env.WORKSPACE;		
    def mvnHome = tool name: 'mvn', type: 'maven';
    env.PATH = "${mvnHome}/bin:${env.PATH}";
    
    env.JAVA_HOME= tool name: 'JDK', type: 'jdk';
    env.PATH="${env.JAVA_HOME}/bin:${env.PATH}";
    sh "cd $workspace; mvn ${mvnTarget}";
}


boolean isReleaseBranch()
{
   	def branchName = env.BRANCH_NAME
    // release branches named major.minor(.bugfix)_releaseName; note: env.BRANCH_NAME is available in multi-branch-pipeline projects
    // Gitflow forces us to use prefix "release/"
    branchName != null && branchName.matches("(release/)?\\d+\\.\\d+(\\.\\d+)?_.*");
}

def setBuildName(){
	def jobName = env.JOB_NAME
	def buildNumber = currentBuild.number
	currentBuild.displayName = "$jobName"+"#"+"$buildNumber"
}